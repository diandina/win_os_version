﻿using System;
using System.Diagnostics;

namespace Win_OS_Version
{
    class Program
    {
        private static string cmdPath = @"C:\Windows\system32\cmd.exe";
        static void Main(string[] args)
        {
            Program program = new Program();
            program.GetOSVersion("winver");
        }
        void GetOSVersion(string cmd)
        {
            Process p = new Process();
            p.StartInfo.FileName = cmdPath;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.StandardInput.WriteLine(cmd);
            p.StandardInput.AutoFlush = true;
                    
            p.WaitForExit();
            p.Close();

        }
    }
}
